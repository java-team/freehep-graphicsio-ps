Source: freehep-graphicsio-ps
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Giovanni Mascellani <gio@debian.org>,
 Philipp Huebner <debalance@debian.org>
Build-Depends:
 debhelper (>= 10),
 default-jdk,
 maven-debian-helper
Build-Depends-Indep:
 junit,
 libfreehep-graphicsio-java,
 libfreehep-graphicsio-tests-java
Standards-Version: 4.1.1
Vcs-Git: https://anonscm.debian.org/git/pkg-java/freehep/freehep-graphicsio-ps.git
Vcs-Browser: https://anonscm.debian.org/git/pkg-java/freehep/freehep-graphicsio-ps.git
Homepage: http://java.freehep.org/

Package: libfreehep-graphicsio-ps-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Recommends: ${maven:OptionalDepends}
Description: FreeHEP (Encapsulated) PostScript Driver
 The GraphicsIO library offers a base interface for image exporters in many
 vector or bitmap image formats. It features direct support for GIF, PNG, PPM
 and RAW formats, as well as the ability to manage TrueType fonts. Support
 for other file types can be added with plugins.
 .
 This package contains a GraphicsIO plugin to export to PostScript format.
 .
 FreeHEP is a collection of Java libraries used in High Energy Physics.
